/**
 * Created by hanni on 11/11/16.
 */
var express = require('express');
var app = express();


var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(express.static(__dirname + '/../public'));

require('./controllers/controller').routes(app);

var server = app.listen('3333', function () {
    console.log('Listen: ' + server.address().port);
});